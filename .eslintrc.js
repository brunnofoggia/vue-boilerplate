module.exports = {
    root: true,

    env: {
        node: true,
    },

    extends: ['plugin:vue/vue3-essential', 'eslint:recommended'],

    parserOptions: {
        ecmaVersion: 2020,
    },

    rules: {
        'no-console': 'off',
        'no-debugger': 'off',
        'no-useless-escape': 'off',
        'vue/multi-word-component-names': 'off',
    },

    overrides: [
        {
            files: [
                '**/__tests__/*.{j,t}s?(x)',
                '**/tests/unit/**/*.spec.{j,t}s?(x)',
            ],
            env: {
                jest: true,
            },
        },
    ],
    "globals": {
        "$": "writable",
        "console": "writable",
        "document": "writable",
        "Event": "writable",
        "Image": "writable",
        "jQuery": "writable",
        "navigator": "writable",
        "process": "writable",
        "Promise": "writable",
        "require": "writable",
        "setTimeout": "writable",
        "setInterval": "writable",
        "clearInterval": "writable",
        "window": "writable",
    }
};
