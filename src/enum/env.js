var list = ['local', 'development', 'staging', 'production'];

var o = {};
list.forEach((v) => (o[v] = v));

export default Object.freeze(o);
