export default {
    namespaced: true,
    state: {
        config: {},
    },
    mutations: {
        config: (state, config) => (state.config = config),
    },
    actions: {
        config(store, config) {
            if (!Object.keys(store.state.config).length)
                store.commit('config', config);
        },
    },
    getters: {
        config(state) {
            return state.config;
        },
    },
};
