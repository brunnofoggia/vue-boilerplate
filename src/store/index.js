import { createStore } from 'vuex';
import VuexPersistence from 'vuex-persist';
import env from './env.store';

var fn = (envConfig) => {
    const vuexLocal = new VuexPersistence({
        key: envConfig.APP_NAME,
        storage: window.localStorage,
        modules: [''],
    });

    const vuexSession = new VuexPersistence({
        key: envConfig.APP_NAME,
        storage: window.sessionStorage,
        modules: [''],
    });

    return createStore({
        namespaced: true,
        state: {},
        mutations: {},
        actions: {},
        modules: { env },
        plugins: [vuexLocal.plugin, vuexSession.plugin],
    });
};

export default fn;
