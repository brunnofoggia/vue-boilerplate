import { createApp } from 'vue';
import env from '@/core/env';
import App from '@/components/views/App/App.vue';
import router from '@/router';
import Store from '@/store';
import VeeValidatePlugin from './includes/vee-validate';
import mixins from '@/mixins';

import jQuery from 'jquery';
window.$ = jQuery;
window.jQuery = window.$;

(async function () {
    var envConfig = (await env.config()).default,
        store = Store(envConfig);
    store.dispatch('env/config', envConfig);

    createApp(App)
        .use(store)
        .use(router)
        .use(VeeValidatePlugin)
        .mixin(mixins)
        .mount('#app');
})();
