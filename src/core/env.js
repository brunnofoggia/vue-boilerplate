import env from '../enum/env';

var o = {};
o.detect = () => {
    var h = window.location.hostname;

    if (
        /localhost/.test(h) ||
        /127\.0\.0\.1/.test(h) ||
        /0\.0\.0\.0/.test(h) ||
        /192\.168\.\d+\.\d+/.test(h)
    ) {
        return env.local;
    }

    var _env = env.production;
    switch (true) {
        case /(\.|_)?(dev|development)\./.test(h):
            _env = env.development;
            break;
        case /(\.|_)?(test|testing)\./.test(h):
            _env = env.testing;
            break;
        case /(\.|_)?(stage|staging|homol|homolog)\./.test(h):
            _env = env.staging;
            break;
    }

    return _env;
};

o.config = async () => {
    var _env = o.detect();
    return import(`@/config/env/${_env}.json`);
    // return new Promise(resolve => {
    //     import(`@/config/env/${_env}.json`, {
    //         assert: {
    //             type: "json",
    //         },
    //     }).then(m => {
    //         console.log(JSON.stringify(m));

    //         resolve({});
    //     });
    // });
};

export default o;
